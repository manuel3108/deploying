const { NodeSSH } = require('node-ssh');

const sevenZipPath = '"C:\\Program Files\\7-Zip\\7z.exe"';
const publishedZipName = 'Nop.zip';

const remoteInnovappsTempDirectory = 'C:\\Innovapps\\deployment\\temp\\';
const iisDirectory = 'C:\\inetpub\\wwwroot\\';
const iisCommandPath = 'C:\\Windows\\system32\\inetsrv\\appcmd';
const appPoolSuffix = 'AppPool';

const host = process.argv[2];
const username = process.argv[3];
const password = process.argv[4];
const applicationName = process.argv[5];
const zipFileName = `../application.zip`;

const ssh = new NodeSSH();

async function doStuff() {
    try {
        // prepare
        await sshConnect();
        await sshUploadNopToServer();

        // extract
        await sshExtractNopFile();

        // delete file
        await sshDeleteNopFile();

        // update file permissions
        await sshUpdateFilePermissions();

        // check if an apppool with the name already exists
        const appPoolExists = await sshCheckAppPool();
        if (!appPoolExists) {
            // if not, create one
            await sshCreateAppPool();
        }

        // check if an site with the name already exists
        const siteExists = await sshCheckSite();
        if (!siteExists) {
            // if not, create one
            await sshCreateSite();
        }

        // stop app pool
        await sshStopAppPool();

        // stop app pool
        await sshStartAppPool();

        // close the ssh connection
        if (ssh.connection) {
            // this function avoids to throw errors, that are caused by a bug in OpenSSH
            // https://github.com/steelbrain/node-ssh/issues/158
            ssh.connection.on('error', function () {
                /* No Op */
            });
            ssh.dispose();
        }

        // wait for the site to become back online
    } catch (error) {
        console.log(error.message);
    }
}

async function sshDeleteNopFile() {
    await ssh.execCommand(
        `del "${remoteInnovappsTempDirectory}\\${publishedZipName}"`
    );
    console.log('Deleted local files');
}

async function sshUpdateFilePermissions() {
    let command = `
    $acl = Get-Acl \\"${iisDirectory}${applicationName}\\";;
    $AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule(\\"IIS_IUSRS\\", \\"FullControl\\", \\"ContainerInherit,ObjectInherit\\", \\"None\\", \\"Allow\\");;
    $acl.SetAccessRule($AccessRule);
    $acl | Set-Acl \\"${iisDirectory}${applicationName}\\";`;
    command = command.replace(/(\r\n|\n|\r)/gm, '');
    await ssh.execCommand(
        `powershell -ExecutionPolicy ByPass -command "${command}"`
    );
    console.log('Finished updating file permissions');
}

async function sshCreateSite() {
    await ssh.execCommand(
        `${iisCommandPath} add site /site.name:"${applicationName}" -applicationDefaults.applicationPool:"${applicationName}-${appPoolSuffix}" /physicalPath:"${iisDirectory}${applicationName}"`
    );
    console.log('Created Site');
}

async function sshCheckSite() {
    const result = await ssh.execCommand(
        `${iisCommandPath} list site /site.name:\"${applicationName}\"`
    );

    return result.stdout !== '';
}

async function sshStopAppPool() {
    await ssh.execCommand(
        `${iisCommandPath} stop apppool /apppool.name:"${applicationName}-${appPoolSuffix}"`
    );
    console.log('Stopped AppPool');
}

async function sshStartAppPool() {
    await ssh.execCommand(
        `${iisCommandPath} start apppool /apppool.name:"${applicationName}-${appPoolSuffix}"`
    );
    console.log('Started AppPool');
}

async function sshCreateAppPool() {
    await ssh.execCommand(
        `${iisCommandPath} add apppool /apppool.name:"${applicationName}-${appPoolSuffix}"`
    );
    console.log('Created AppPool');
}

async function sshCheckAppPool() {
    const result = await ssh.execCommand(
        `${iisCommandPath} list apppool /apppool.name:\"${applicationName}-${appPoolSuffix}\"`
    );

    return result.stdout !== '';
}

async function sshExtractNopFile() {
    await ssh.execCommand(
        `${sevenZipPath} x "${remoteInnovappsTempDirectory}${publishedZipName}" -o"${iisDirectory}${applicationName}"`
    );
    console.log('Extracted file');
}

async function sshUploadNopToServer() {
    console.log('Uploading file');
    await ssh.putFile(
        zipFileName,
        `${remoteInnovappsTempDirectory}\\${publishedZipName}`
    );
    console.log('Uploaded File');
}

async function sshConnect() {
    console.log('Connecting to remote host...');
    await ssh.connect({
        host,
        username,
        password,
    });
    console.log('Connected');
}

doStuff();
